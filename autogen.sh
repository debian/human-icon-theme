#!/bin/sh

intltoolize -c --force		|| exit 1
aclocal				|| exit 1
automake --gnu --add-missing	|| exit 1
autoconf			|| exit 1
echo configure will be run, if you want to pass arguments, add them to ./autogen.sh
./configure $@

